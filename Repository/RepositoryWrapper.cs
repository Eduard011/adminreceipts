﻿using Contracts;
using Entities;

namespace Repository
{
    public class RepositoryWrapper : IRepositoryWrapper
    {
        private RepositoryContext _repositoryContext;
        private IProviderRepository _provider;
        private ICurrencyRepository _currency;
        private IReceiptRepository _receipt;

        public IProviderRepository Provider
        {
            get
            {
                if (_provider == null)
                {
                    _provider = new ProviderRepository(_repositoryContext);
                }

                return _provider;
            }
        }

        public ICurrencyRepository Currency
        {
            get
            {
                if (_currency == null)
                {
                    _currency = new CurrencyRepository(_repositoryContext);
                }

                return _currency;
            }
        }

        public IReceiptRepository Receipt
        {
            get
            {
                if (_receipt == null)
                {
                    _receipt = new ReceiptRepository(_repositoryContext);
                }

                return _receipt;
            }
        }

        public RepositoryWrapper(RepositoryContext repositoryContext)
        {
            _repositoryContext = repositoryContext;
        }

        public void Save()
        {
            _repositoryContext.SaveChanges();
        }
    }
}
