﻿using Contracts;
using Entities;
using Entities.Models;

namespace Repository
{
    public class ReceiptRepository : RepositoryBase<Receipt>, IReceiptRepository
    {
        public ReceiptRepository(RepositoryContext repositoryContext) : base(repositoryContext)
        {

        }
    }
}
