﻿using Contracts;
using Entities;
using Entities.Models;

namespace Repository
{
    public class ProviderRepository : RepositoryBase<Provider>, IProviderRepository
    {
        public ProviderRepository(RepositoryContext repositoryContext) : base(repositoryContext)
        {

        }
    }
}
