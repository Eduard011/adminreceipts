﻿using Contracts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace AdminReceipts.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private readonly ILoggerManager _logger;
        private readonly IRepositoryWrapper _repositoryWrapper;

        public WeatherForecastController(ILoggerManager logger, IRepositoryWrapper repositoryWrapper)
        {
            _logger = logger;
            _repositoryWrapper = repositoryWrapper;
        }

        [HttpGet]
        public IEnumerable<string> Get()
        {
            _logger.LogInfo("Mensaje info desde el controlador");
            _logger.LogDebug("Mensaje debug desde el controlador");
            _logger.LogWarn("Mensaje warn desde el controlador");
            _logger.LogError("Mensaje error desde el controlador");

            var providers = _repositoryWrapper.Provider.FindAll();
            var currencies = _repositoryWrapper.Currency.FindAll();
            var receipts = _repositoryWrapper.Receipt.FindAll();

            return new string[] { "value1", "value2" };
        }
    }
}
