﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contracts
{
    public interface IRepositoryWrapper
    {
        IProviderRepository Provider { get; }
        ICurrencyRepository Currency { get; }
        IReceiptRepository Receipt { get; }
        void Save();
    }
}
