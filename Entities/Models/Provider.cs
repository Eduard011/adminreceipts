﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    [Table("providers")]
    public class Provider
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "El nombre del proveedor es requerido")]
        [StringLength(50, ErrorMessage = "El nombre no debe de contener mas de 50 caracteres")]
        public string Name { get; set; }

    }
}
