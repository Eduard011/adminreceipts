﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    [Table("currencies")]
    public class Currency
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "El nombre de la moneda es requerido")]
        [StringLength(50, ErrorMessage = "El nombre de la moneda no debe de contener mas de 50 caracteres")]
        public string Name { get; set; }

        [Required(ErrorMessage = "La abreviacion de la moneda es requerida")]
        [StringLength(5, ErrorMessage = "La abreviacion de la moneda no debe de contener mas de 5 caracteres")]
        public string Abbreviation  { get; set; }
    }
}
