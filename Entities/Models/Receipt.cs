﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    [Table("receipts")]
    public class Receipt
    {
        public int ID { get; set; }

        public Provider Provider { get; set; }

        public decimal Amount { get; set; }

        public Currency Currency { get; set; }

        public DateTime CreatedAt { get; set; }

        public string Commentary { get; set; }
    }
}
